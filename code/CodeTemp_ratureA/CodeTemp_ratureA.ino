#include <WiFi.h>
#include <U8x8lib.h> // Affichage Console
#include "ThingSpeak.h"
#include <Wire.h> // driver I2C
#include "SparkFunHTU21D.h"



U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(15,4,16); // clock, data, reset
HTU21D myTempHumi; // température

// CONSTANTES DIVERSES
const char *title = "Detecteur_T";
const int delayLoop = 2000; // En millisecondes
const int nbChannelTemperature = 1;
const int nbChannelHumidity = 3;

// CONSTANTES de connexion 4g
char ssid[] = "domestiques"; // Network SSID (name)
char pass[] = "42424242"; // Network Password

// CONSTANTES de connexion à ThingSpeak
unsigned long myChannelNumber = 766937;
const char * myWriteAPIKey = "7N640O6EK7DJIU0M" ;

WiFiClient client;


// Affichage console
void afficheData(float humidity, float temperature) {
    char dbuf[32];
    u8x8.clear();

    Serial.println(title);
    u8x8.drawString(0,1,title); // 0 – colonne (max 15), 1 – ligne (max 7)

    sprintf(dbuf, "Hum : %f", humidity); // Humidité
    u8x8.drawString(0,2,dbuf);

    sprintf(dbuf, "Temp : %f", temperature); // Température
    u8x8.drawString(0,3,dbuf);

    delay(6000);
}


// Setup
void setup() {
    Serial.begin(9600);
    Wire.begin(21,22);  // carte d'extension I2C2
    //Wire.begin(22,21);  // carte d'extension I2C1

    myTempHumi.begin();

    // Console
    u8x8.begin(); // Initialisation OLED
    u8x8.setFont(u8x8_font_chroma48medium8_r);

    // WiFi
    WiFi.disconnect(true); // effacer de l’EEPROM les WiFi crédentials
    delay(1000);
    WiFi.begin(ssid, pass);
    delay(1000);
    while (WiFi.status() != WL_CONNECTED) { // Tant que l'on arrive pas à se connecter, nous affichons "." sur la console
        delay(500);
        Serial.print(".");
    }
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);
    Serial.println("WiFi setup ok");
    delay(1000);

    // Connexion ThingSpeak
    ThingSpeak.begin(client); // connexion (TCP) du client au serveur
    delay(1000);
    Serial.println("ThingSpeak begin");
}


// Loop s'éxécute en boucle
void loop()
{
    // Lectures
    float humidity = myTempHumi.readHumidity();
    float temperature = myTempHumi.readTemperature(); 

    // Affichage console
    afficheData(humidity, temperature);
    // Préparation des champs
    ThingSpeak.setField(nbChannelTemperature, temperature);
    ThingSpeak.setField(nbChannelHumidity, humidity);


    while (WiFi.status() != WL_CONNECTED) {
        delay(500);Serial.print(".");
    }

    // Ecriture des champs préparés
    ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
    
    delay(delayLoop);
}
