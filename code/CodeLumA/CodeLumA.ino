#include <WiFi.h>
#include <U8x8lib.h> // Affichage Console
#include "ThingSpeak.h"
#include <Wire.h> // driver I2C
#include "SparkFunHTU21D.h"
#include <BH1750.h>

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(15,4,16); // clock, data, reset
BH1750 lightMeter;


// CONSTANTES DIVERSES
const char *title = "Detecteur Lum";
const int delayLoop = 2000; // En millisecondes
const int nbChannelTemperature = 1;
const int nbChannelLuminosity = 2;
const int seuilLuminosite = 200; // Au delà de 400 lux, on considère qu'il fait jour

// CONSTANTES de connexion 4g
char ssid[] = "domestiques"; // Network SSID (name)
char pass[] = "42424242"; // Network Password

// CONSTANTES de connexion à ThingSpeak
unsigned long myChannelNumber = 766937;
const char * myWriteAPIKey = "7N640O6EK7DJIU0M" ;
const char * myReadAPIKey = "MYZE985TQTFKCTK4" ;

WiFiClient client;

// Affichage console
void afficheData(float luminosity) {
    char dbuf[64];
    u8x8.clear();

    Serial.println(title);
    u8x8.drawString(0,1,title); // 0 – colonne (max 15), 1 – ligne (max 7)

    sprintf(dbuf, "Lum : %f", luminosity); // Luminosité
    u8x8.drawString(0,2,dbuf);

    sprintf(dbuf, "Il fait ");
    u8x8.drawString(0,3,dbuf);
    if(luminosity > seuilLuminosite) {
        sprintf(dbuf, "jour");
    } else {
        sprintf(dbuf, "nuit");
    }
    u8x8.drawString(0,4,dbuf);
    
    delay(6000);
}

void afficheTemperature(float temperature) {
    char dbuf[32];
    // On ne clear pas afin de ne pas effacer les informations de luminosité déjà affichée

    Serial.println("Température : " + String(temperature));

    sprintf(dbuf, "Temp : %f", temperature); // Température
    u8x8.drawString(0,5,dbuf);

    delay(6000);
}

// Setup
void setup() {
    Serial.begin(9600);
    Wire.begin(21,22);  // carte d'extension I2C2
    //Wire.begin(22,21);  // carte d'extension I2C1

    lightMeter.begin();

    // Console
    u8x8.begin(); // Initialisation OLED
    u8x8.setFont(u8x8_font_chroma48medium8_r);

    // WiFi
    WiFi.disconnect(true); // effacer de l’EEPROM les WiFi crédentials
    delay(1000);
    WiFi.begin(ssid, pass);
    delay(1000);
    while (WiFi.status() != WL_CONNECTED) { // Tant que l'on arrive pas à se connecter, nous affichons "." sur la console
        delay(500);
        Serial.print(".");
    }
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);
    Serial.println("WiFi setup ok");
    delay(1000);

    // Connexion ThingSpeak
    ThingSpeak.begin(client); // connexion (TCP) du client au serveur
    delay(1000);
    Serial.println("ThingSpeak begin");
}





// Loop s'éxécute en boucle
void loop()
{
    // Lectures
    uint16_t lux = lightMeter.readLightLevel();

    // Affichage console
    afficheData(lux);


    while (WiFi.status() != WL_CONNECTED) {
        delay(500);Serial.print(".");
    }

    if(lux > seuilLuminosite) {
        // Lecture de la température stockée sur le serveur ThingSpeak
        float temperature = ThingSpeak.readFloatField(myChannelNumber, nbChannelTemperature, myReadAPIKey);

        // On vérifie si la lecture s'est bien passée
        int statusCode = ThingSpeak.getLastReadStatus();
        if(statusCode == 200) {
            afficheTemperature(temperature);
        }

        // Préparation des champs
        ThingSpeak.setField(nbChannelLuminosity, lux);
        // Ecriture des champs préparés
        ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
    }
    
    
    delay(delayLoop);
}
